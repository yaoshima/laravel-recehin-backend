<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'fullname' => 'Anthony',
            'email' => 'aumerusai@gmail.com',
            'phone_number' => '6282312603896',
            'balance' => 1000,
            'security_code' => '123456'
        ]);

        User::create([
            'fullname' => 'Nana',
            'email' => 'nana@gmail.com',
            'phone_number' => '6282612605796',
            'balance' => 200,
            'security_code' => '123456'
        ]);
    }
}
