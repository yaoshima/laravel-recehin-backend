<?php

use Illuminate\Database\Seeder;
use App\Voucher;

class VouchersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Voucher::create([
            'provider' => 'Genki Shusi',
            'name' => 'Sushi murah untuk 2 orang',
            'price' => 25000,
            'quantity' => 15,
            'expired_date' => '2018-12-28',
            'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
            'tems_condition' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur',
            'banner' => 'https://s3-ap-southeast-1.amazonaws.com/auction-thesis/banner-genki.jpg',
            'logo' => 'https://s3-ap-southeast-1.amazonaws.com/auction-thesis/logo-genki.jpg'
        ]);

        Voucher::create([
            'provider' => 'Hoka hoka bento',
            'name' => 'Tempura hemat buat 2 orang',
            'price' => 80000,
            'quantity' => 50,
            'expired_date' => '2018-12-21',
            'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
            'tems_condition' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur',
            'banner' => 'https://s3-ap-southeast-1.amazonaws.com/auction-thesis/banner-hoka.jpg',
            'logo' => 'https://s3-ap-southeast-1.amazonaws.com/auction-thesis/logo-hoka.jpg'
        ]);

        Voucher::create([
            'provider' => 'Tawan',
            'name' => 'Main course hemat',
            'price' => 150000,
            'quantity' => 5,
            'expired_date' => '2018-12-16',
            'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters',
            'tems_condition' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur',
            'banner' => 'https://s3-ap-southeast-1.amazonaws.com/auction-thesis/banner-tawan.jpg',
            'logo' => 'https://s3-ap-southeast-1.amazonaws.com/auction-thesis/logo-tawan.jpg'
        ]);

    }
}
