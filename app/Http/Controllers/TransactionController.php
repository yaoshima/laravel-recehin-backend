<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transaction;
use App\Voucher;


class TransactionController extends Controller
{
    public function buyVoucher(Request $request){
    	$buyVoucherData = $request->json()->all();
    	$user = User::find($buyVoucherData['user_id']);
    	$voucher = Voucher::find($buyVoucherData['voucher_id']);

    	if($user->balance < $voucher->price){
    		return response()->json(['message' => 'Woops, dana kamu tidak mencukupi', 'user' => $user, 'voucher' => $voucher]);
    	}
    	else{
			$trx = Transaction::create([
		        'user_id' => $buyVoucherData['user_id'],
		        'event' => 'Deal purchase',
		        'party' => $voucher->provider,
		        'operation' => 'subtract',
		        'amount' => $voucher->price
		    ]);

		    $user->balance -= $voucher->price;
		    $user->save();

		    $voucher->quantity -= 1;
		    $voucher->save();

		    return response()->json(['message' => 'Berhasil membeli voucer' . $voucher->name, 'user' => $user, 'voucher' => $voucher]);
    	}
    }

    public function addBalance(Request $request){
    	$addBalanceData = $request->json()->all();
    	$user = User::find($addBalanceData['user_id']);

    	$trx = Transaction::create([
		        'user_id' => $addBalanceData['user_id'],
		        'event' => 'Recieve Balance',
		        'party' => 'Retailer',
		        'operation' => 'add',
		        'amount' => $addBalanceData['balance']
		    ]);

    	$user->balance += $addBalanceData['balance'];
    	$user->save();

    	return response()->json(['message' => 'Berhasil menambahkan balance sebesar ' . $addBalanceData['balance'], 'user' => $user]);

    }

    public function userTransaction(User $user){
    	$transactions = Transaction::latest()
    								->where('user_id',$user->id)
    								->get()
    								->groupBy(function($item){
								    	return $item->created_at->format('d M Y');
								    });

    	$trx = [];
		foreach ($transactions as $date => $transaction) {
			$obj = (object)[];
			$obj->date = $date;
			$obj->transactions = [];
			foreach ($transaction as $depTrx) {
				$obj->transactions[] = $depTrx;
			}
			$trx[] = $obj;
		}
    	return response()->json($trx);
    	//dd($trx);
    }
}
