<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

class UserApiController extends Controller
{
    //
    public function register(Request $request){
    	$registerData = $request->json()->all();
    	$user = User::create([
            'fullname' => $registerData['fullname'],
            'email' => $registerData['email'],
            'phone_number' => $registerData['phone_number'],
            'balance' => 0,
            'security_code' => '121212'
        ]);
    	return response()->json(['message' => 'success']);
    }

    public function login(Request $request){
    	// $loginData = $request->json()->all();
    	// $phoneNumber = $loginData['phone_number'];
    	// $isUserExist = User::where('phone_number',$phoneNumber)->exists();
    	// if(!$isUserExist){
    	// 	return response()->json(['message' => 'failed']);
    	// }
    	// else{
    	// 	$random  = rand(1000,9999);
    	// 	$sender  = 'Recehin';
    	// 	$message = 'Your OTP is : ' . $random;

    	// 	$client = new Nexmo\Client(new Nexmo\Client\Credentials\Basic('91949252','2Z4cDzmdOxdK8zDo'));
    	// 	$message = $client->message()->send([
    	// 	'to' => $phoneNumber, 
    	// 	'from' => $sender,	
    	// 	'text' => $message
    	// 	]);

    	// 	return response()->json(['message' => 'success']);
    	// }

    	$loginData = $request->json()->all();
    	$phoneNumber = $loginData['phone_number'];
    	$isUserExist = User::where('phone_number',$phoneNumber)->exists();
    	
    	if($isUserExist){
    		$user = User::where('phone_number',$phoneNumber)->first();

    		// $random  = rand(1000,9999);
    		// $sender  = 'Recehin';
    		// $message = 'Your OTP is : ' . $random;

    		// $client = new Nexmo\Client(new Nexmo\Client\Credentials\Basic('37463b5d','U55nbOO87LIJZbkL'));
    		// $message = $client->message()->send([
    		// 'to' => $phoneNumber, 
    		// 'from' => $sender,	
    		// 'text' => $message
    		// ]);

    		return response()->json(['user' => $user, 'error' => '0']);
    	}
    	else{
    		return response()->json(['message' => 'Phone number not registered yet', 'error' => '1']);
    		
    	}

    }
}
