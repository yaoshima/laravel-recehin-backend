<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Voucher;

class VoucherController extends Controller
{
    //

    public function index(){
    	$vouchers = Voucher::all();
    	/*$vouchersJSON = response(Voucher::all()->jsonSerialize(), Response::HTTP_OK);
    	dd($vouchersJSON);*/
    	return response()->json($vouchers);
    }

    public function show(Voucher $voucher){
    	return response()->json($voucher);
    }
}
