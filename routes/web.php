<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/register', 'UserApiController@register');
Route::post('/login', 'UserApiController@login');

Route::get('/vouchers', 'VoucherController@index');
Route::get('/vouchers/{voucher}', 'VoucherController@show');

Route::post('/buy/voucher', 'TransactionController@buyVoucher');

Route::post('/add/balance', 'TransactionController@addBalance');

Route::get('/user/transaction/{user}', 'TransactionController@userTransaction');
